require "spec_helper"

describe MoviesController do
  render_views
  describe "MoviesController - Sort on Title, Release Date & Filter by Ratings\n" do
  before :each do
    movies = [
        {title: 'Aladdin', rating: 'G', release_date: '25-Nov-1992'},
        {title: 'The Terminator', rating: 'R', release_date: '26-Oct-1984'},
        {title: 'When Harry Met Sally', rating: 'R', release_date: '21-Jul-1989'},
        {title: 'The Help', rating: 'PG-13', release_date: '10-Aug-2011'},
        {title: 'Chocolat', rating: 'R', release_date: '5-Jan-2001'},
        {title: 'Amelie', rating: 'R', release_date: '25-Apr-2001'},
        {title: '2001: A Space Odyssey', rating: 'G', release_date: '6-Apr-1968'},
        {title: 'The Incredibles', rating: 'PG', release_date: '5-Nov-2004'},
        {title: 'Raiders of the Lost Ark', rating: 'PG', release_date: '12-Jun-1981'},
        {title: 'Chicken Run', rating: 'G', release_date: '21-Jun-2000'}
    ]

    # store movies for each rating separately
    @my_movies = {'G' => [], 'R' => [], 'PG-13' => [], 'PG' => []}
    # Create above movies list in the database
    movies.each_index do |i|
      movie = Movie.create!(movies[i])
      @my_movies[movie.rating] << movie.title
    end
  end

  # Helper for setting up Check boxes state
  def check_ratings(ratings, mark)
    ratings.each do |r|
      id = "ratings_#{r}"
      (mark)? check(id): uncheck(id)
    end
  end

  # Helper for checking presence/absence of movies with certain ratings
  def check_for_movies(ratings, criteria)
    result = true
    count = 0
    ratings.each do |r|
      @my_movies[r].each do |title|
        if (criteria == :present && page.body.include?(title) == true) ||
            (criteria == :absent && page.body.include?(title) == false)
          count = count + 1
        end
      end
      result = result && (@my_movies[r].count == count)
      count = 0
    end
    result.should == true
  end

  context  "On the Home page, Filter by ratings" do
    before :each do
      @all_ratings = %w[PG R PG-13 G]
      visit "/movies"
    end
    it "should restrict to movies with 'PG' or 'R' ratings" do
      check_ratings %w[PG R], true
      check_ratings %w[PG-13 G], false
      click_button "Refresh"
      check_for_movies(%w[PG R], :present)
      check_for_movies(%w[PG-13 G], :absent)
    end

    it "should handle no ratings selected" do
      check_ratings @all_ratings, false
      click_button "Refresh"
      check_for_movies(@all_ratings, :absent)
    end

    it "should handle all ratings selected" do
      check_ratings @all_ratings, true
      click_button "Refresh"
      check_for_movies(@all_ratings, :present)
    end
  end

  context "On the Home page, sort movies"  do
    before :each do
      visit "/movies"
    end
    it "should sort on Movie Title" do
      click_link "Movie Title"
      page.body.should =~ /Aladdin.*Amelie/m
      page.body.should =~ /2001: A Space Odyssey.*Aladdin/m
      page.body.should =~ /Aladdin.*Amelie/m
      page.body.should =~ /Amelie.*Chicken Run/m
      page.body.should =~ /Chicken Run.*Chocolat/m
      page.body.should =~ /Chocolat.*Raiders of the Lost Ark/m
      page.body.should =~ /Raiders of the Lost Ark.*The Help/m
      page.body.should =~ /The Help.*The Incredibles/m
      page.body.should =~ /The Incredibles.*The Terminator/m
      page.body.should =~ /The Terminator.*When Harry Met Sally/m
    end
    it "should sort on Release Date" do
      click_link "Release Date"
      page.body.should =~ /2001: A Space Odyssey.*Raiders of the Lost Ark/m
      page.body.should =~ /Raiders of the Lost Ark.*The Terminator/m
      page.body.should =~ /The Terminator.*When Harry Met Sally/m
      page.body.should =~ /When Harry Met Sally.*Aladdin/m
      page.body.should =~ /Aladdin.*Chicken Run/m
      page.body.should =~ /Chicken Run.*Chocolat/m
      page.body.should =~ /Chocolat.*Amelie/m
      page.body.should =~ /Amelie.*The Incredibles/m
      page.body.should =~ /The Incredibles.*The Help/m
    end
  end
end # ends "sort & filter"

describe "MoviesController - Add director field, list movies with same director" do
  before :each do
    @movies = Hash.new
    @movies['Star Wars'] = Movie.create(title: 'Star Wars', rating: 'PG', director: 'Geoge Lucas', release_date: '1977-05-25')
    @movies['Blade Runner'] = Movie.create(title: 'Blade Runner', rating: 'PG', director: 'Ridley Scott', release_date: '1982-06-25')
    @movies['Alien'] = Movie.create(title: 'Alien', rating: 'R', director: '', release_date: '1979-05-25')
    @movies['THX-1138'] = Movie.create(title: 'THX-1138', rating: 'R', director: 'Geoge Lucas', release_date: '1971-03-11')
  end

  context "Add director to existing movie 'Alien':\n" do
    #Setup movie Alien and visit its Edit Movie Page
    before :each do
      movie_id = @movies['Alien'].id
      edit_page = "/movies/#{movie_id}/edit"
      visit edit_page
    end
    it "should have edit page for 'Alien'" do
      page.body.should have_selector("h1", :content => "Edit Existing Movie")
      page.body.should have_selector("#movie_title", :content => "Alien")
    end
    it "should have Director field in Edit page"  do
      page.body.should have_selector('#movie_director')
    end
    it "should be possible to update Director field and see it in details page" do
      fill_in "Director", :with => "Ridley Scott"
      click_button "Update Movie Info"
      response.should render_template("show")   # Details page for 'Alien'
      regexp = /Director:\s*Ridley Scott/m
      page.body.should =~ regexp
    end
  end

  context "Find movies with same director\n" do
    # setup movie 'Star Wars' and visit its Details page
    before :each do
      @results = [@movies['Star Wars'], @movies['THX-1138']]
      @movie_id = @movies['Star Wars'].id
      details_page = "/movies/#{@movie_id}"
      visit details_page
    end
    it "should have a 'Find Movies With Same Director' link in Details Page"  do
      page.body.should have_link('Find Movies With Same Director')
    end
    it "should call model method to find all movies with same director" do
      Movie.should_receive(:find_all_with_same_director).with(@movie_id.to_s).and_return(@results)
      post :with_same_director, :id => @movie_id    # URI - /movies/:id/with_same_director
    end
    it "should have a method and template for Find Movies With Same Director" do
      post :with_same_director, :id => @movie_id    # URI - /movies/:id/with_same_director
      response.should render_template('with_same_director')
    end
    it "should make Similar Movies result available to that template" do
      post :with_same_director, :id => @movie_id    # URI - /movies/:id/with_same_director
      response.should render_template('with_same_director')
      assigns(:movies).should == @results
      assigns(:movies).should_not include @movies['Blade Runner']
    end
  end

  context "can't find similar movies if we don't know director (sad path)\n"  do
    before :each do
      @movie_id = @movies['Alien'].id
      details_page = "/movies/#{@movie_id}"
      visit details_page
    end
    it "should not have 'Ridley Scott' in details page for 'Alien'" do
      regexp = /Alien.*Ridley Scott/m
      page.body.should_not =~ regexp
    end
    it "should default to home page when director info is absent" do
      click_link "Find Movies With Same Director"
      response.should render_template('index')
      page.body.should have_selector("h1", :content => 'All Movies')
    end
    it "should inform about failure to find movies with same director, when director info is absent" do
      click_link "Find Movies With Same Director"
      page.body.should include "'Alien' has no director info"
    end
  end
end # ends "director and similar movies"

describe "MoviesController - Destroy" do
  before :each do
    @movies = Hash.new
    @movies['Star Wars'] = Movie.create(title: 'Star Wars', rating: 'PG', director: 'Geoge Lucas', release_date: '1977-05-25')
    @movies['Blade Runner'] = Movie.create(title: 'Blade Runner', rating: 'PG', director: 'Ridley Scott', release_date: '1982-06-25')
    @movies['Alien'] = Movie.create(title: 'Alien', rating: 'R', director: '', release_date: '1979-05-25')
    @movies['THX-1138'] = Movie.create(title: 'THX-1138', rating: 'R', director: 'Geoge Lucas', release_date: '1971-03-11')
  end

  context "Delete movie\n" do
    it "delete Star Wars" do
      visit "/movies/#{@movies['Star Wars'].id}"
      click_button "Delete"
      # now it has the 'Star Wars' deleted message
      page.body.should have_selector("#notice", :content => "'Star Wars' was deleted")
      visit "/movies"
      page.body.should_not include 'Star Wars'
    end
  end
end # ends "delete method calls"
end




