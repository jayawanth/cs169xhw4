Given /the following movies exist/ do |movies_table|
  movies_table.hashes.each do |movie|
    movie_record = Movie.create!(movie)
  end
end

Then /the director of "(.*)" should be "(.*)"/ do |movie, this_director|
  reg_exp = /#{movie}(.*)Director:\s*#{this_director}/m
  page.body.should =~ reg_exp
end

